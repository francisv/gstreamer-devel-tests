/* GStreamer
 * Copyright (C) 2010 Stefan Kost <ensonic@users.sf.net>
 *
 * capsnego.c: benchmark for caps negotiation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/* This benchmark recursively builds a pipeline and measures the time to go
 * from READY to PAUSED state.
 *
 * The graph size and type can be controlled with a few command line options:
 *
 *  -d depth: is the depth of the tree
 *  -c children: is the number of branches on each level
 *  -f <flavour>: can be "audio" or "video" and is controlling the kind of
 *                elements that are used.
 */

#include <gst/gst.h>
#include <stdlib.h>
#include <string.h>

static void
event_loop (GstElement * bin)
{
  GstBus *bus;
  GstMessage *msg = NULL;
  gboolean running = TRUE;

  bus = gst_element_get_bus (bin);

  while (running) {
    msg = gst_bus_poll (bus,
        GST_MESSAGE_ASYNC_DONE | GST_MESSAGE_ERROR | GST_MESSAGE_WARNING, -1);

    switch (GST_MESSAGE_TYPE (msg)) {
      case GST_MESSAGE_ASYNC_DONE:
        running = FALSE;
        g_print ("async");
        break;
      case GST_MESSAGE_WARNING:{
        GError *err = NULL;
        gchar *dbg = NULL;

        gst_message_parse_warning (msg, &err, &dbg);
        GST_WARNING_OBJECT (GST_MESSAGE_SRC (msg), "%s (%s)", err->message,
            (dbg ? dbg : "no details"));
        g_clear_error (&err);
        g_free (dbg);
        g_print ("warning");
        break;
      }
      case GST_MESSAGE_ERROR:{
        GError *err = NULL;
        gchar *dbg = NULL;

        gst_message_parse_error (msg, &err, &dbg);
        GST_ERROR_OBJECT (GST_MESSAGE_SRC (msg), "%s (%s)", err->message,
            (dbg ? dbg : "no details"));
        g_clear_error (&err);
        g_free (dbg);
        running = FALSE;
        g_print ("error");
        break;
      }
      default:
        g_print ("default");
        break;
    }
    // gst_message_unref (msg);
  }
  // gst_object_unref (bus);
}

gint
main (gint argc, gchar * argv[])
{
  /* default parameters */
  gint loops = 10;

  GOptionContext *ctx;
  GOptionEntry options[] = {
    {"loops", 'l', 0, G_OPTION_ARG_INT, &loops,
        "How many loops to run (default: 50)", NULL}
    ,
    {NULL}
  };
  GError *err = NULL;
  GstBin *bin;
  GstClockTime start, end;
  gint i;

  g_set_prgname ("capsnego");

  /* check command line options */
  ctx = g_option_context_new ("");
  g_option_context_add_main_entries (ctx, options, NULL);
  g_option_context_add_group (ctx, gst_init_get_option_group ());
  if (!g_option_context_parse (ctx, &argc, &argv, &err)) {
    g_print ("Error initializing: %s\n", GST_STR_NULL (err->message));
    g_clear_error (&err);
    g_option_context_free (ctx);
    return 1;
  }
  g_option_context_free (ctx);

  /* build pipeline */
  start = gst_util_get_timestamp ();
  bin = GST_BIN (gst_parse_launch ("playbin3", NULL));
  if (bin == NULL) {
    goto Error;
  }

  g_object_set (G_OBJECT (bin), "uri",
      "file:///home/francisv/media/Hydrate-Kenny_Beltrey.ogg", NULL);

  end = gst_util_get_timestamp ();
  /* num-threads = num-sources = pow (children, depth) */
  g_print ("%" GST_TIME_FORMAT " built pipeline with %d elements\n",
      GST_TIME_ARGS (end - start), GST_BIN_NUMCHILDREN (bin));

  /* measure */
  g_print ("starting pipeline\n");
  gst_element_set_state (GST_ELEMENT (bin), GST_STATE_READY);
  GST_DEBUG_BIN_TO_DOT_FILE (bin, GST_DEBUG_GRAPH_SHOW_ALL, "capsnego");

  start = gst_util_get_timestamp ();
  for (i = 0; i < loops; ++i) {
    gst_element_set_state (GST_ELEMENT (bin), GST_STATE_PAUSED);
    event_loop (GST_ELEMENT (bin));
    gst_element_set_state (GST_ELEMENT (bin), GST_STATE_READY);
  }
  end = gst_util_get_timestamp ();

  g_print ("%" GST_TIME_FORMAT " reached PAUSED state (%d loop iterations)\n",
      GST_TIME_ARGS (end - start), loops);

/* clean up */
Error:
  g_print ("Error*********\n");
  gst_element_set_state (GST_ELEMENT (bin), GST_STATE_NULL);
  //gst_object_unref (bin);
  return 0;
}
