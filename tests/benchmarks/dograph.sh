#!/bin/sh

CONFIGURATION=$1
DOT_FILE="graphs/capsnego.dot"
dot -Tsvg $DOT_FILE > graphs/$CONFIGURATION.svg
google-chrome graphs/$CONFIGURATION.svg
