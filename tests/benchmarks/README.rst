=================
 Of my interest:
=================

- ``caps.c``: benchmark for caps creation and destruction
- ``capsnego.c``: benchmark for caps negotiation.
  Options are::

    -c number of SINK pads of video mixer
    -d length of path composed by mixer ! scale ! convert
    -l loops 

  For example::

    USE_PLAYBIN3=1 GST_DEBUG="GST_TRACER:9" GST_TRACERS="stats;rusage" GST_DEBUG_FILE=trace-capsnego.log ./capsnego -c 1 -d 0 -f video -l 1
    python /home/francisv/gst/master/gst-tracer-stats-tools/gsttracer-negotiation-analyzer.py trace-capsnego.log
    gst-stats-1.0 trace-capsnego.log

Other examples ``gsttracer-negotiation-analyzer.py``::

    USE_PLAYBIN3=1 GST_DEBUG="GST_TRACER:9" GST_TRACERS="stats;rusage" GST_DEBUG_FILE=trace-playbin.log gst-play-1.0 file:///home/francisv/media/Hydrate-Kenny_Beltrey.ogg
    python /home/francisv/gst/master/gst-tracer-stats-tools/gsttracer-negotiation-analyzer.py trace.log

- ``complexity.c``:  I run it like::

    ./complexity.scm 20

  This is equivalent to run::

    ./complexity 1 20
    ./complexity 2 20
    ./complexity 4 20
    ./complexity 8 20
    ./complexity 16 20

  Then::

    gnuplot complexity.gnuplot

  The result is in ``complexity.ps``.

- I have no managed to run ``mas-elements.scm`` yet.

